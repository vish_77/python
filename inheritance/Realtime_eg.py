

#Tell this example in interview 

class Company:
    #class variable:
    cname = "Veritas"
    workplace = "Baner"

    #Constructor
    def __init__(self):
        print("Company Constructor")
        self.teamcode = "Python code"

    @classmethod
    def facilities(cls):
        print("Provides all facilities ...")
        print(cls.cname)
        print(cls.workplace)

class Employee(Company):
    #class variable
    roll = "Developer"

    def __init__(self,empId,lang):
        super(). __init__()
        print("Child constructor")
        self.empId = empId
        self.lang = lang

    def info(self):
        print(self.empId)
        print(self.roll)
        print(self.lang)
        print(self.workplace)

    @classmethod
    def skillset(cls):
        print(cls.roll)

emp1 = Employee(25,"Pythond Developer")
emp2 = Employee(35,"Java Developer")

emp1.info()
emp2.info()
