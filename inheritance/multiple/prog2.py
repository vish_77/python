class N :
    def __init__(self):
        super().__init__()
        print("N constructor")

class O :
    def __init__(self):
        #super().__init__()
        print("O  constructor")


class M(N) :
    def __init__(self):
        super().__init__()
        print("M constructor")

class I(M) :
    def __init__(self):
        super().__init__()
        print("I constructor")

class H(M) :
    def __init__(self):
        super().__init__()
        print("H constructor")

class D(H,I) :
    def __init__(self):
        super().__init__()
        print("D  constructor")

class L(O) :
    def __init__(self):
        super().__init__()
        print("L constructor")

class G(L) :
    def __init__(self):
        super().__init__()
        print("G constructor")

class K(O) :
    def __init__(self):
        super().__init__()
        print("K constructor")

class F(K) :
    def __init__(self):
        super().__init__()
        print("F constructor")

class B(F,G) :
    def __init__(self):
        super().__init__()
        print("B constructor")


class J(O) :
    def __init__(self):
        super().__init__()
        print("j constructor")

class E(J) :
    def __init__(self):
        super().__init__()
        print("E constructor")

class A(E) :
    def __init__(self):
        super().__init__()
        print("A  constructor")

class C(A,B,D) :
    def __init__(self):
        super().__init__()
        print("C constructor")

obj = C()
print(C.mro())

