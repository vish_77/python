
class Parent:
    x = 10
    def show(self):
        print(self.x)

class child(Parent):
    x = 20
    def show(self):
        print(self.x)
        print(Parent.x)
        super().show()
        print(super().x)
obj = child()
obj.show()
