class Parent:
    x=10
    @classmethod
    def show(cls):
        print(cls.x)
class child(Parent):
    x=30
    def __init__(self):
        self.x=20
    def show(self):
        print(self.x)
        super().show()
        print(Parent.x)
    #print(super(child()))
obj = child()

obj.show()

print(super(child))
