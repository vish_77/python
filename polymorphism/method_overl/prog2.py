

from multipledispatch import dispatch

class Addition:

    @dispatch(int,int)
    def add(self,x,y):
        return x+y

    @dispatch(int,int,int)
    def add(self,x,y,z):
        return x+y+z

    @dispatch(float,float)
    def add(self,x,y):
        return x+y

    @dispatch(int,float)
    def add(self,x,y):
        return x+y

    @dispatch(float,float,float)
    def add(self,x,y,z):
        return x+y+z

obj = Addition()
obj.add(5,5)

