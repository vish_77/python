

"""Write a Meta class for witch do not accept more than 1 class variable """



class MetaOne(type):

    def __new__(cls,clsname,basecls,clsdict):
  
       print(clsname)
       print(clsdict)

       if (len(clsdict) == 4):
           raise TypeError ("More than one class variable not allowed ")
   
       return super().__new__(cls,clsname,basecls,clsdict)

class A(metaclass = MetaOne):

    x = 10
    y = 20
    z = 30
class B(A):

    p = 40
    q = 50

print(type(A))
print(type(B))

