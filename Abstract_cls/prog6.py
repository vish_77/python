
""" Abstract Property =>
     
    - in this abstract property child class has completion that data has retrun form method 
    - If we declare any method as a abstract property then it will call as a variable i.e without parenthesis
"""

from abc import ABC,abstractmethod,abstractproperty


class Company(ABC):

    @abstractmethod
    def info(self):
        None

    @abstractproperty
    def name(self):
        None

class Employee(Company):
    
    def __init__(self,empname):
        self.ename = empname

    def info(self):
        print(self.ename)

    @property
    def name(self):
        return self.ename

obj = Employee("Kunal")
obj.info()
print(obj.name)
