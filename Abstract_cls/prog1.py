from abc import ABC,abstractmethod

class College(ABC):

    @abstractmethod
    def placement(self):
        print("Any company")

    def degree(self):
         print("B.Tech")
    
    @abstractmethod
    def teaching(self):
        pass
class Stud(College):
    
    def placement(self):
        print("Veritas")
    
    def degree(self):
        print("Fail")

    def teaching(self):
        print("Teachers are not teaching well")

obj=Stud()
obj.placement()
obj.degree()
obj.teaching()
print(dir(Stud))

