

#Normal inner class 

class Hostel:

    parking = "Yes"

    def __init__(self):

        self.rooms = 50
        self.beds = 150

    class Room:

        fan = "yes"

        def __init__(self):

            self.bed = 3
            self.phone = 3
            self.laptop = 3
        
        def disp(self):
            print("bed = {} , phone = {} , laptop = {}". format(self.bed, self.phone, self.laptop))

        @classmethod
        def dispFan(cls):
            print(cls.fan)

    def disp(self):
        print(self.rooms)
        print(self.beds)
        print(self.parking)
        
host = Hostel()
host.disp()

room = host.Room()  #Creating object like this is good
room.disp()

room1 = Hostel().Room()
room1.disp()
