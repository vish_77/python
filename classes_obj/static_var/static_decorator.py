


" static method decorator"

class Cric:

    matchformat = "T20"

    def __init__(self):
        self.name = "Dhoni"
        self.jerNo = 7

    #decorator
    def mystaticmethod(fun):
        def inner(*args):
            fun()
        return inner

    #instance method 
    def disp(self):
        print(self.name)
        print(self.jerNo)

    @mystaticmethod
    def despFormat():
        print()

player1 = Cric()
player1.disp()
player1.despFormat()
