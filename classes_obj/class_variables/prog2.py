class Cafe:

    menucard = 1

    def __init__(self,dish):
        self.dish = dish

    def order(self):
        print(self.dish)
        print(self.menucard)

obj1 = Cafe("Burger")
obj2 = Cafe("Large Fries")

obj1.order()

Cafe.menucard = 0

obj2.order()
