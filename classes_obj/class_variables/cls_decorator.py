



" Decorator code for class method "

class Cric:
    matchformat = "T20"

    def __init__ (self):

        self.name = "Dhoni"
        self.jerNo = 7

    #decorator
    def myclassmethod(fun):

        def inner (*args):
            fun(args[0].__class__)

        return inner
    #instance method
    def disp(self):
        print(self.name,self.jerNo)
        print("name = {} and jerNO = {}".format(self.name,self.jerNo))

    #classmethod
    @myclassmethod
    def dispFormat(cls):
        print(cls)
        print(cls.matchformat)

player1 = Cric()
player1.disp()

print(player1)

player1.dispFormat()
